"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var router = express.Router();
router.post('/home', () => {
    console.log('just came home!');
    const { Sonos } = require('sonos');
    const device = new Sonos('192.168.0.14');
    device.play()
        .then(() => console.log('now playing'));
    device.getVolume()
        .then((volume) => console.log(`current volume = ${volume}`));
});
router.post('/left', () => {
    console.log('Niklas has left the building!');
    console.log('just came home!');
    const { Sonos } = require('sonos');
    const device = new Sonos('192.168.0.14');
    device.stop()
        .then(() => console.log('now playing'));
    device.getCurrentStatus()
        .then((status) => console.log(`current status = ${status}`));
});
module.exports = router;
//# sourceMappingURL=api.js.map