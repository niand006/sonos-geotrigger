var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();

const { DeviceDiscovery } = require('sonos');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Custom route namespaces
var api = require('./routes/api.js');
app.use('/api', api);
var gui = require('./routes/gui.js');
app.use('/gui', gui);

// catch 404 and forward to error handler
app.use(function (req: Request, res: Response, next: Function) {
    var err: any = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err: any, req: any, res: any, next: Function) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

// event on all found...
DeviceDiscovery((device: any) => {
  console.log('found device at ' + device.host)

  // mute every device...
//   device.setMuted(true)
//     .then(`${device.host} now muted`)
}).catch((e: Error) => {
    console.error('caught zones for each error again: ' + e);
});

// find one device
DeviceDiscovery().once('DeviceAvailable', (device: any) => {
  console.log('found device at ' + device.host)

  // get topology
  device.getTopology()
    .then(console.log)
})

module.exports = app;