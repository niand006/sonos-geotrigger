var express = require('express');
var router = express.Router();

router.get('/', function(req: Request, res: any, next: Function) {
  res.render('index');
});
router.get('/settings', function(req: Request, res: any, next: Function) {
  res.render('settings');
});

module.exports = router;