export { };  // needs to be treated as a module due to interfering global declarations

var express = require('express');
var router = express.Router();

router.post('/home', () => {
    console.log('just came home!');
    const { Sonos } = require('sonos');

    const device = new Sonos('192.168.0.14');

    device.play()
        .then(() => console.log('now playing'))

    device.getVolume()
        .then((volume: any) => console.log(`current volume = ${volume}`))
});

router.post('/left', () => {
    console.log('Niklas has left the building!');
    console.log('just came home!');
    const { Sonos } = require('sonos');

    const device = new Sonos('192.168.0.14');

    device.stop()
        .then(() => console.log('now playing'))

    device.getCurrentStatus()
        .then((status: any) => console.log(`current status = ${status}`))

    
});

module.exports = router;